﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models
{
    public static class StockPricingFactory
    {
        private static Random randomNo = new Random();

        private static List<string> stockCodes = new List<string>(new string[]
                                {"AAE", "AAM", "AAN", "AAO", "AAQ", "AAR", "AAU", "AAX", "ABB", "ABC",
                                 "ABI", "ABJ", "AAC", "ABP", "ABQ", "ABS", "ABU", "ABY", "ACB", "ACE",
                                 "ACL", "ACR", "ADB", "ADG", "ADI", "ADL", "ADN", "ADS", "ADU", "ADX",
                                 "ADY", "ADZ", "AEC", "AED", "AEE", "AEI", "AEO", "AEP", "AET", "AEU",
                                 "AEX", "AEZ", "AFG", "AFI", "AFT", "AGC", "AGF", "AGG", "AGI", "AGK"
                                });

        #region Public Properties

        /// <summary>
        /// Gets the List of available Stock Codes to populate the Users dropdown list.
        /// Note: A dropdown list was used to aid in UI useability. No mention on requirement 
        /// for future proofing (when Stock Code list items exceeds 50 items)
        /// </summary>
        public static List<string> ListOfStockCodes
        {
            get { return stockCodes; }
        }

        /// <summary>
        /// Get a Stock Price (ie. it's just a random number 0 - 100)
        /// </summary>
        public static double StockPrice
        {
            get { return randomNo.NextDouble() * 100; }
        }

        #endregion
    }
}
